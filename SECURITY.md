# Security Policy

## Supported Versions

Security updates are normally only applied only to the most recent release. Depending of the severity of issue and adoption of new major releases, it may be back-ported to legacy releases.

## Reporting a Vulnerability

To securely report a vulnerability, please open a project issue using the [`Security` issue template](https://gitlab.com/gitlab-ci-utils/references/gitlab-group-project-summary/-/issues/new?issuable_template=Security). Once submitted the issue will be confidential and only visible to the author and project members.

Please do not open a merge request for a vulnerability since it will publicly disclose the details.

## Vulnerability Process

The following process details how vulnerabilities will be addressed:

- The issue will be acknowledged within 2 business days.
- The issues will be assessed with the relevant information. Addition information may be requested, as required.
- If the security issue is confirmed, any vulnerabilities will be formally documented as [CVEs](https://about.gitlab.com/security/cve/), and action will be taken to remediate them.
  - At this point, the issue confidentiality may be removed to make the issue publicly visible.
- If the security issues is not confirmed, that will be noted in the issue. If applicable, details will be captured in relevant documentation.
