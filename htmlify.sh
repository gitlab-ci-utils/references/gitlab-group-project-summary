#!/bin/sh

# If specified, use RELEASE_YEAR, otherwise take current year
if [ -z "$RELEASE_YEAR" ]; then
  year=$(date +%Y)
else
  year=$RELEASE_YEAR
fi
file="projects-$year.md"

# Insert frontmatter for conversion to HTML
title="Project Release Report for $year"
frontmatter="---\ntitle: $title\n---\n"
sed -i "1s/^/$frontmatter/" "$file"

# Ensure pandoc is installed
apk add pandoc

# Convert to HTML using template file
mkdir -p public
pandoc "projects-$year.md" --template=template.html -o "./public/index.html"
