import { markdownTable } from 'markdown-table';

/**
 * Generates a markdown formatted table with new projects in the given year.
 * Includes a link to the project and the project description.
 *
 * @param   {object[]} projects The projects to summarize.
 * @param   {number}   year     The year to summarize.
 * @returns {string}            The new project summary table.
 */
const getProjectSummaryByYear = (projects, year) => {
    const projectSummary = [['Project', 'Description']];
    projectSummary.push(
        ...projects
            .filter(
                (project) =>
                    project.createdAt.getFullYear() === year &&
                    project.description
            )
            .map((project) => [
                `[${project.displayName}](https://gitlab.com/${project.fullPath})`,
                `${project.description}`
            ])
    );
    return markdownTable(projectSummary, {
        align: ['l', 'l'],
        alignDelimiters: false
    });
};

/**
 * Generates a markdown formatted table of releases for a given year.
 * Has link to project, total count of releases, and a list of the releases
 * with links to the release page.
 *
 * @param   {object[]} projects The projects to summarize.
 * @param   {number}   year     The year to summarize.
 * @returns {string}            The releases for the year.
 */
// eslint-disable-next-line max-lines-per-function -- long due to formatting
const getReleasesByYear = (projects, year) => {
    let releaseCount = 0;
    const releaseTypes = {
        major: 0,
        minor: 0,
        patch: 0,
        preRelease: 0,
        zero: 0
    };

    const projectsForYear = [['Project', 'Releases']];
    for (const project of projects) {
        const releases = project.releases.filter(
            (release) => release.releasedAt.getFullYear() === year
        );
        if (releases.length > 0) {
            releaseCount += releases.length;
            const releaseList = releases
                .map((release) => {
                    releaseTypes[release.type] += 1;
                    return `[${release.tagName}](https://gitlab.com/${project.fullPath}/-/releases/${release.tagName})`;
                })
                .join(', ');

            projectsForYear.push([
                `[${project.displayName}](https://gitlab.com/${project.fullPath})`,
                `**${releases.length}** (${releaseList})`
            ]);
        }
    }
    const tableMarkdown = markdownTable(projectsForYear, {
        align: ['l', 'l'],
        alignDelimiters: false
    });

    return (
        `${tableMarkdown}\n\nTotal Releases: ${releaseCount} ` +
        `(${releaseTypes.major} major, ` +
        `${releaseTypes.minor} minor, ` +
        `${releaseTypes.patch} patch, ` +
        `${releaseTypes.preRelease} pre-release, ` +
        `${releaseTypes.zero} zero)`
    );
};

/**
 * Generates a markdown report listing:
 * - The new project created that year.
 * - The releases for each project for that year.
 *
 * @param   {object[]} projects The projects to summarize.
 * @param   {number}   year     The year to summarize.
 * @returns {string}            The markdown summary report.
 */
const getProjectReleaseReportByYear = (projects, year) => {
    const report = [];

    report.push(
        `# Project Release Report for ${year}`,
        '',
        `## New Projects in ${year}`,
        '',
        getProjectSummaryByYear(projects, year),
        '',
        `## Project Releases in ${year}`,
        '',
        getReleasesByYear(projects, year),
        ''
    );

    return report.join('\n');
};

export { getProjectReleaseReportByYear };
