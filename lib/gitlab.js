const httpStatusError = 400;

const filterGroup = (group) => ({
    // Assumes only one level of subgroups, removes parent group name
    displayName: group.name === group.full_name ? '' : group.name,
    id: Number(group.id),
    name: group.full_name,
    path: group.full_path,
    webUrl: group.web_url
});

/**
 * Compares two values to determine a sort order, sorting by the first field
 * then the second field.
 *
 * @param   {string} field1 The name of the first field to compare.
 * @param   {string} field2 The name of the second field to compare.
 * @returns {number}        The sort order.
 * @private
 */
// eslint-disable-next-line complexity -- allow low threshold
const sortByFields = (field1, field2) => (a, b) => {
    if ((a[field1] !== '' && b[field1] === '') || a[field1] < b[field1]) {
        return -1;
    }
    if ((a[field1] === '' && b[field1] !== '') || a[field1] > b[field1]) {
        return 1;
    }

    if (a[field2] < b[field2]) {
        return -1;
    }
    if (a[field2] > b[field2]) {
        return 1;
    }

    return 0;
};

/**
 * Fetch a URL from the GitLab API. Add the instance prefix and a suffix to
 * manage paging.
 *
 * @param   {string}            url     The URL without the instance prefix or
 *                                      a suffix.
 * @param   {object}            options Standard fetch options.
 * @returns {Promise<Response>}         Promising resolving a fetch response.
 */
// eslint-disable-next-line require-await -- wrapper for fetch, returns a promise
const fetchInternal = async (url, options) => {
    const gitLabApiUrl = 'https://gitlab.com/api/v4';
    const gitLabApiSuffix = '?per_page=100';
    return fetch(`${gitLabApiUrl}${url}${gitLabApiSuffix}`, options);
};
/**
 * Get group data by group ID.
 *
 * @param   {number} id The GitLab group ID.
 * @returns {object}    The group data.
 */
const getGroupById = async (id) => {
    const groupResponse = await fetchInternal(`/groups/${id}`);
    const group = await groupResponse.json();
    return filterGroup(group);
};

const getSubgroupsForGroup = async (group) => {
    const groupsResponse = await fetchInternal(`/groups/${group.id}/subgroups`);
    const subgroups = await groupsResponse.json();
    return subgroups.map((subgroup) => filterGroup(subgroup));
};

/**
 * Get group and all subgroup data by group ID.
 *
 * @param   {number}   id The GitLab group ID.
 * @returns {object[]}    The group data.
 */
const getGroupWithSubgroupsById = async (id) => {
    const results = [];
    const parentGroup = await getGroupById(id);
    results.push(parentGroup, ...(await getSubgroupsForGroup(parentGroup)));
    return results;
};

/**
 * Get project data for a group.
 *
 * @param   {object}   group The group data.
 * @returns {object[]}       The project data.
 */
const getProjectsForGroup = async (group) => {
    const projectsResponse = await fetchInternal(
        `/groups/${group.id}/projects`
    );
    const projects = await projectsResponse.json();
    return projects
        .filter((project) => !project.archived)
        .map((project) => ({
            containerRegistryEnabled: project.container_registry_enabled,
            createdAt: new Date(project.created_at),
            defaultBranch: project.default_branch,
            description: project.description,
            displayName: group.displayName
                ? `${group.displayName} / ${project.name}`
                : project.name,
            fullName: project.name_with_namespace,
            fullPath: project.path_with_namespace,
            id: Number(project.id),
            name: project.name,
            namespaceDisplayName: group.displayName,
            packagesEnabled: project.packages_enabled,
            path: project.path,
            webUrl: project.web_url
        }))
        .sort(sortByFields('namespaceDisplayName', 'name'));
};

/**
 * Get project data for an array of groups.
 *
 * @param   {object[]} groups The groups to get projects data for.
 * @returns {object[]}        The project data.
 */
const getProjectsForGroups = async (groups) => {
    const allProjects = await Promise.all(
        groups.map(async (group) => await getProjectsForGroup(group))
    );
    return allProjects
        .flat()
        .sort(sortByFields('namespaceDisplayName', 'name'));
};

const getReleaseType = (tag) => {
    const formattedTag = tag.replace('v', '');
    if (formattedTag.includes('-')) {
        return 'preRelease';
    }
    if (formattedTag.startsWith('0')) {
        return 'zero';
    }
    if (formattedTag.endsWith('.0.0')) {
        return 'major';
    }
    if (formattedTag.endsWith('.0')) {
        return 'minor';
    }
    return 'patch';
};

/**
 * Gets all releases for the given project.
 *
 * @param   {object}   project The project to check for releases.
 * @returns {object[]}         The releases for the given project.
 */
const getReleasesForProject = async (project) => {
    const releasesResponse = await fetchInternal(
        `/projects/${project.id}/releases`
    );
    // GitLab returns 403 if no releases exist
    if (releasesResponse.status >= httpStatusError) {
        return [];
    }
    const releases = await releasesResponse.json();
    return releases.map((release) => ({
        description: release.description,
        name: release.name,
        releasedAt: new Date(release.released_at),
        tagName: release.tag_name,
        type: getReleaseType(release.tag_name)
    }));
};

/**
 * Gets all releases for the given array of projects.
 *
 * @param   {object[]} projects The project to check for releases.
 * @returns {object[]}          The releases for the given project.
 */
const getReleasesForProjects = async (projects) =>
    await Promise.all(
        projects.map(async (project) => {
            const releases = await getReleasesForProject(project);
            return { ...project, releases };
        })
    );

export {
    getGroupById,
    getGroupWithSubgroupsById,
    getProjectsForGroup,
    getProjectsForGroups,
    getReleasesForProject,
    getReleasesForProjects
};
