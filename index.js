import { writeFile } from 'node:fs/promises';

// NOTE: All API functions only retrieve the first page of 100 results and
// do not handle pagination. This is a known limitation of this app.

import {
    getGroupWithSubgroupsById,
    getProjectsForGroups,
    getReleasesForProjects
} from './lib/gitlab.js';
import { getProjectReleaseReportByYear } from './lib/reports.js';

const yearLength = 4;

/**
 * Get year from ISO 8601 formatted date.
 *
 * @param   {string} isoDate Date in ISO 8601 format.
 * @returns {string}         Year extracted from the date.
 * @private
 */
const getYearFromIsoDate = (isoDate) => isoDate.slice(0, yearLength);

/**
 * Gets the year to use for the report. Defaults to the pipeline execution year
 * if not explicitly set in the environment variable REPORT_YEAR.
 *
 * @returns {number} The year to use for the report.
 * @private
 */
const getReportYear = () =>
    Number(
        process.env.REPORT_YEAR ||
            getYearFromIsoDate(process.env.CI_PIPELINE_CREATED_AT)
    );

/**
 * Gets the namespace ID to use for the report. Defaults to the project's parent
 * if not explicitly set in the environment variable REPORT_NAMESPACE_ID.
 *
 * @returns {number} The namespace ID to use for the report.
 * @private
 */
const getReportNamespaceId = () =>
    Number(
        process.env.REPORT_NAMESPACE_ID || process.env.CI_PROJECT_NAMESPACE_ID
    );

/**
 * Gets all project/release data for a group and its subgroups from the
 * GitLab API. Assumes data is all public.
 *
 * @param   {string}            parentGroupId The namespace ID of the group.
 * @returns {Promise<object[]>}               An array of project data with release data.
 * @private
 */
const getProjectGroupData = async (parentGroupId) => {
    console.log(
        `Getting group/project data from GitLab for namespace ID ${parentGroupId}...`
    );
    const allGroups = await getGroupWithSubgroupsById(parentGroupId);
    const allProjects = await getProjectsForGroups(allGroups);
    return getReleasesForProjects(allProjects);
};

/**
 * Generates report and saves to a file.
 *
 * @param {object[]} allProjectsWithReleases An array of project data with
 *                                           release data.
 * @param {string}   year                    The year to filter by.
 * @private
 */
const generateReport = async (allProjectsWithReleases, year) => {
    console.log(`Generating report for year ${year}...`);
    const fileName = `projects-${year}.md`;
    await writeFile(
        fileName,
        getProjectReleaseReportByYear(allProjectsWithReleases, year)
    );
    console.log(`Wrote report to ${fileName}`);
};

try {
    const year = getReportYear();
    const parentNamespaceId = getReportNamespaceId();
    const allProjectsWithReleases =
        await getProjectGroupData(parentNamespaceId);
    await generateReport(allProjectsWithReleases, year);
} catch (error) {
    console.error(`Error generating report: ${error}`);
    process.exitCode = 1;
}
