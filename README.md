# GitLab Group Project Summary

Use GitLab API to generate a report of new projects and releases in a
specified group and year.

## Assumptions

- The group and projects are public, so no API token is required.
- The group can have **_one_** tier of subgroups, but no more.
- GitLab API paging isn't currently supported, so:
  - The total number of subgroups in the group is less than or equal to 100.
  - The total number of projects in the group and each subgroup is less than
    or equal to 100.
  - The total number of releases in each project is less than or equal to 100.
- Projects with no description are not reported.

## Usage

The report is generate by running `npm ci && npm start` in the root directory
of the project. The report is saved in the current working directory as
`projects-2024.md` (with the applicable year).

The report is run for the following data:

- If specified, the namespace specified in variable `REPORT_NAMESPACE_ID` is
  used. Otherwise, the predefined variable `CI_PROJECT_NAMESPACE_ID` is used.
- If specified, the year in variable `REPORT_YEAR` is used. Otherwise, the year
  from predefined variable `CI_PIPELINE_CREATED_AT` is used.

The report for the current year for the `gitlab-ci-utils` group (ID 5407807) is
created and posted to
[GitLab pages](https://gitlab-ci-utils.gitlab.io/references/gitlab-group-project-summary/).
