# Changelog

## Unreleased

### Fixed

- Updated table width to use 100% of available width.

## v1.0.4 (2024-11-11)

### Changed

- Updated `engines:node` to `^22.11.0`, which became the Active LTS on
  2024-10-29. (#6)

## v1.0.3 (2024-10-22)

### Fixed

- Updated to `markdown-table@3.0.4`.

## v1.0.2 (2024-09-21)

### Fixed

- Updated report to display subgroups with projects and sort with subgroups.
  (#3)
- Fixed error using default namespace when `REPORT_NAMESPACE_ID` not
  specified. (#4)

### Miscellaneous

- Fixed `rules` for `npm_check` job to not run on daily update schedule
  pipelines.
- Updated `build_report` job to use `.node` template for consistency.

## v1.0.1 (2024-08-30)

### Fixed

- Updated `pages` settings to use deterministic URLs for the report.

### Miscellaneous

- Updated `schedule` pipelines to allow failure for vulnerability scans if
  daily update.

## v1.0.0 (2024-08-29)

Initial release
