import recommendedConfig from '@aarongoldenthal/eslint-config-standard/recommended-esm.js';

export default [
    ...recommendedConfig,
    {
        rules: {
            'node/no-unsupported-features/node-builtins': 'off'
        }
    },
    {
        ignores: ['.vscode/**', 'archive/**', 'node_modules/**', 'coverage/**'],
        name: 'ignores'
    }
];
